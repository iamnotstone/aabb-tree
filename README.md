# 相对于原来的库，增加了根据shape来找该shape与AABBTree中相交的结点

# 原来的库地址 https://github.com/JeremyAube/AABBTreejs.git


# added two functions compare to origin project
1. AABBTree.GetShapesOverlappingWithPoint(point: Vector3 | Vector2)
2. AABBTree.GetShapesOverlappingWithShape(another: IAABBShape)

# AABB Tree
Simple implementation of an AABB Tree (Axis Aligned Bounding Box Tree) to optimize collision detection

## Documentations
The TypeDoc generated documentation can be found at the following [here](https://jeremyaube.github.io/AABBTreejs/)

## Commands
**Run Test**: `npm test`  
**Generate the docs**: `npm docs`  
**Build the package**: `npm build`

## Ressources
- https://www.azurefromthetrenches.com/introductory-guide-to-aabb-tree-collision-detection/
- https://www.randygaul.net/2013/08/06/dynamic-aabb-tree/

## Contributing
Feel free to fork this project and send your changes in a pull requests.

## Bugs
If you find a bug, please report it as a Github Issue.
